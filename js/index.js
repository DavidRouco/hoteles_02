$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 2000 
			});

			//Evento show.bs.modal del div Modal Contacto:  class="modal fade" id="contacto"
			$('#contacto').on('show.bs.modal', function (e){
				console.log('El modal contacto se está mostrando.'); //F12/Console
				
				//Cambiamos el color eliminando y añadiendo class
				$('#contactoBtn').removeClass('btn-outline-success');
				$('#contactoBtn').removeClass('btn-primary'); //solo de prueba lo esta añadiendo siempre.
				//Desactivamos el boton
				$('#contactoBtn').prop('disabled', true);

			});
			//Evento shown.bs.modal del div Modal Contacto:  class="modal fade" id="contacto"
			$('#contacto').on('shown.bs.modal', function (e){
				console.log('El modal contacto se mostró.'); //F12/Console
			});
			//Evento hide.bs.modal del div Modal Contacto:  class="modal fade" id="contacto"
			$('#contacto').on('hide.bs.modal', function (e){
				console.log('El modal contacto se está ocultando.'); //F12/Console
			});
			//Evento hidden.bs.modal del div Modal Contacto:  class="modal fade" id="contacto"
			$('#contacto').on('hidden.bs.modal', function (e){
				console.log('El modal contacto ocultó.'); //F12/Console
				//Activamos el boton
				$('#contactoBtn').prop('disabled', false);

			});


		});